import { EntryCollection, Entry } from 'contentful';
import { IPost, IRawPost, contentFulPostToPostMapper } from '../models/IPost';
import IProject, { IRawProject, contentFulProjectToProjectMapper } from '../models/IProject';
import client from '../plugins/contentful';

export default class ContentfulFetcher {
  private static postTypeId = 'post';
  private static projectTypeId = 'project';

  public static async getPosts(filters?: any): Promise<IPost[]> {
    const options = {
      'content_type': this.postTypeId,
      ...filters
    }
    const response: EntryCollection<IRawPost> = await client.getEntries(options)
    
    return response.items
      .map((item: Entry<IRawPost>) => contentFulPostToPostMapper(item));
  }

  public static async getPost(filters?: any): Promise<IPost> {
    const response: EntryCollection<IRawPost> = await client
      .getEntries<IRawPost>({
        content_type: this.postTypeId,
        ...filters
      });

    return response.items
      .map((item: Entry<IRawPost>) => contentFulPostToPostMapper(item))[0];
  }

  public static async getProjects(filters?: any): Promise<IProject[]> {
    const options = {
      'content_type': this.projectTypeId,
      ...filters
    }
    const response: EntryCollection<IRawProject> = await client.getEntries(options)
    
    return response.items
      .map((item: Entry<IRawProject>) => contentFulProjectToProjectMapper(item));
  }

  public static async getProject(filters?: any): Promise<IProject> {
    const response: EntryCollection<IRawProject> = await client
      .getEntries<IRawProject>({
        content_type: this.projectTypeId,
        ...filters
      });

    return response.items
      .map((item: Entry<IRawProject>) => contentFulProjectToProjectMapper(item))[0];
  }
}