import { VuexModule, Module, Mutation, Action, getModule } from "vuex-module-decorators";
import { store } from "~/store";
import ContentfulFetcher from '~/middlewares/ContentfulFetcher';
import { IPost } from "../models/IPost";

@Module({ dynamic: true, store, name: "Post", namespaced: true })
class Post extends VuexModule {
  // state
  private posts: IPost[] = [];

  // getters
  get getPosts() {
    return this.posts;
  }

  // mutations
  @Mutation
  setPosts(posts: any[]) {
    this.posts = [...posts];
  }

  // actions
  @Action
  async fetchPosts() {
    const posts: IPost[] = await ContentfulFetcher.getPosts();
    this.setPosts(posts);
  }
}

export default getModule(Post);