import { VuexModule, Module, Mutation, Action, getModule } from "vuex-module-decorators";
import { store } from "~/store";

@Module({ dynamic: true, store, name: "IndexPage", namespaced: true })
class UserModule extends VuexModule {
  // state
  firstName = "Foo";
  lastName = "Bar";

  // getters
  get fullName() {
    return this.firstName + " " + this.lastName;
  }

  // mutations
  @Mutation
  setFirstName(firstName: string) {
    this.firstName = firstName;
  }
  @Mutation
  setLastName(lastName: string) {
    this.lastName = lastName;
  }

  // actions
  @Action
  async loadUser() {
    // const user = await fetchUser();
    // this.setFirstName(user.firstName);
    // this.setLastName(user.lastName);
  }
}

export default getModule(UserModule);