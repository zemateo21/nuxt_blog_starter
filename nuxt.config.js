import ContentfulFetcher from './middlewares/ContentfulFetcher';
import dotenv from "dotenv";
dotenv.config();

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'NUXT_BLOG_STARTER',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/scss/_variables.scss',
    '~/layouts/_fonts.scss',
    '~/assets/scss/main.scss'
  ],

  env: {
    CONTENTFUL_SPACE: process.env.CONTENTFUL_SPACE,
    CONTENTFUL_ACCESSTOKEN: process.env.CONTENTFUL_ACCESSTOKEN,
    CONTENTFUL_ENVIRONMENT: process.env.CONTENTFUL_ENVIRONMENT
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/contentful",
    '~/plugins/prism'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/style-resources'
  ],

  styleResources: {
    scss: [
      '~assets/scss/_variables.scss',
      '~assets/scss/_fonts.scss',
      '~assets/scss/main.scss'
    ]
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // generate: {
  //   async routes() {
  //     return Promise.all([
  //       await ContentfulFetcher.getPosts()
  //     ])
  //       .then(([blogEntries]) => {
  //         console.log("🚀 / file: nuxt.config.js / line 89 / .then / blogEntries", blogEntries)
  //         return [...blogEntries.map(entry => entry.slug)];
  //       })
  //       .then();
  //   }
  // },

  generate: {
    async routes () {
      const posts = ContentfulFetcher
        .getPosts()
        .then(post => post.slug = '/')

      return Promise.all([posts])
        .then(values => values.join().split(','));

      // const posts = createClient({
      //   space: apiConfig.space,
      //   accessToken: apiConfig.accessToken
      // }).getEntries({
      //   content_type: apiConfig.contentTypes.posts
      // })
      // .then(function (entries) {
      //   return entries.items.map((item) => {
      //     return item.fields.slug + '/'
      //   })
      // })

      // const categories = createClient({
      //   space: apiConfig.space,
      //   accessToken: apiConfig.accessToken
      // }).getEntries({
      //   content_type: apiConfig.contentTypes.categories
      // })
      // .then(function (entries) {
      //   return entries.items.map((item) => {
      //     return 'category/' + slugify(item.fields.title) + '/'
      //   })
      // })

      // return Promise.all([posts, categories]).then(values => {
      //   return values.join().split(',')
      // })
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
