import { Entry } from "contentful";

export default interface IProject {
  name: string
}

export interface IRawProject {}

export function contentFulProjectToProjectMapper(projectItem: Entry<IRawProject>): IProject {
  return {} as IProject
}