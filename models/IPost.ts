import { Asset, Entry } from "contentful";
import { BLOCKS, INLINES } from "@contentful/rich-text-types";
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';

export interface IRawPost {
  title: string,
  slug: string,
  heroPic: Asset,
  description: string,
  content: any,
  date: Date,
  author: any,
  category: any,
}

export interface IPost {
  title: string,
  slug: string,
  heroPic: {
    fileName: string,
    url: string
  },
  content: any,
  date: Date,
  author: { id: number, name: string },
  category: { slug: number, name: string },
  description: string
}

export function contentFulPostToPostMapper(postItem: Entry<IRawPost>): IPost {
  return {
    title: postItem.fields.title,
    slug: postItem.fields.slug,
    heroPic: {
      fileName: postItem.fields.heroPic.fields.file.fileName,
      url: postItem.fields.heroPic.fields.file.url
    },
    description: postItem.fields.description,
    content: documentToHtmlString(postItem.fields.content, postRenderOptions),
    date: postItem.fields.date,
    author: {
      id: postItem.fields.author.fields.id,
      name: postItem.fields.author.fields.name
    },
    category: {
      slug: postItem.fields.category.fields.slug,
      name: postItem.fields.category.fields.name
    },
  }
}

const postRenderOptions = {
  renderNode: {
    // [BLOCKS.PARAGRAPH]: (node: any, children: any) => {
    //   return `<p class="post_content_p">${node.content[0].value}</p>`
    // },
    [BLOCKS.EMBEDDED_ENTRY]: (node: any) => {
      let render = ''
      if (node.data.target.sys.contentType.sys.id === "codeBlock") {
        if (node.data.target.fields.language === 'html') {
          node.data.target.fields.code = node.data.target.fields.code.replace(/</g, '&lt;');
        }
        render = `
          <pre class="language-${node.data.target.fields.language}" tabindex="0">
            <code class="language-${node.data.target.fields.language}">${node.data.target.fields.code}</code>
          </pre>`
      }
      return render;
    }
  }
}