import * as contentful from 'contentful';

const space = process.env.CONTENTFUL_SPACE || '';
const accessToken = process.env.CONTENTFUL_ACCESSTOKEN || '';

export default contentful.createClient({ space, accessToken });
