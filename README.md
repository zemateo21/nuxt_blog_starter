# NUXT_BLOG_STARTER

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).


### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).

### `SASS`
- install dependencies compatible with Webpack 4.46: npm i -D sass-loader@^10 sass
- install files loader: npm i @nuxtjs/style-ressources
- in nuxt.config.js:
  - add '@nuxtjs/style-resources' to buildModules
  - add
      ```
        css: [
          '~/assets/scss/_variables.scss',
          '~/assets/scss/_fonts.scss',
          '~/assets/scss/main.scss'
        ],
        styleResources: {
          scss: [
            '~assets/scss/_variables.scss',
            '~assets/scss/_fonts.scss',
            '~assets/scss/main.scss'
          ]
        },
      ```
  - and create assets/scss folder with files in it

### `TYPESCRIPT DECORATORS`
- install dependencies: npm install vue-property-decorator vuex-class vuex-class-modules -P
  - vue-class-component used to define components which is installed by default when Typescript Vue 3 project created
  - vue-property-decorator used to define props, watches, etc.
  - vuex-class used to import state, getters, mutations and actions in components
  - vuex-class-modules used to define state, getters, mutations and actions

### `ENV VARIABLES`
- add following in nuxt.config.js:
```
import dotenv from "dotenv";
dotenv.config();
```
- and in export default:
```
  env: {
    CONTENTFUL_SPACE: process.env.CONTENTFUL_SPACE,
    CONTENTFUL_ACCESSTOKEN: process.env.CONTENTFUL_ACCESSTOKEN,
    CONTENTFUL_ENVIRONMENT: process.env.CONTENTFUL_ENVIRONMENT
  },
```

### `PRISMJS`
- npm i prismjs
- add prisms.js in plugins folder
```
import Prism from 'prismjs'
import 'prismjs/themes/prism-tomorrow.css' // You can add other themes if you want
export default Prism
```
- then in component:
  - import Prism from '~/plugins/prism'
  - Prism.highlightAll() in mounted()
  - <div v-html="post.long"></div>

### `CONTENTFUL`
- add "~/plugins/contentful" in plugins in nuxt.config.js
- create contentful.js in plugins folder
```
import * as contentful from 'contentful';

const space = process.env.CONTENTFUL_SPACE || '';
const accessToken = process.env.CONTENTFUL_ACCESSTOKEN || '';

export default contentful.createClient({ space, accessToken });
```
- install npm install @contentful/rich-text-html-renderer to render Rich Text Content

### `DEPLOY ON NETLIFY VIA GITLAB`
- Add new site / Import an existing project
- log to gitlab and choose repository
- Build settings:
  - Base directory: leave empty
  - Build command: npm run generate
  - Publish directory: dist